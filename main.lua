-----------------------------------------------------------------------------------------
--
-- main.lua
-- Spencer Bowen
-- Austin Vickers
--
-----------------------------------------------------------------------------------------

local widget = require("widget");
local isTouchMode = true;
local sliderCover;

--Setting up sprite options
local imgOpts = 
{
  frames = {
    { x = 13 , y = 11, width = 147 , height = 67 },
	{ x = 13 , y = 95 , width = 54 , height = 39 },
	{ x = 71 , y = 95 , width = 55 , height = 35 },
	{ x = 130 , y = 95 , width = 50 , height = 43 },
	{ x = 185 , y = 95 , width = 50 , height = 52 },
	{ x = 14 , y = 153 , width = 64 , height = 48 },
	{ x = 86 , y = 153 , width = 57 , height = 48 },
	{ x = 149 , y = 153 , width = 54 , height = 48 },
	{ x = 211 , y = 153 , width = 56 , height = 48 },
	{ x = 274 , y = 153 , width = 48 , height = 48 },
	{ x = 15 , y = 220 , width = 48 , height = 24 },
	{ x = 70 , y = 220 , width = 48 , height = 20 },
	{ x = 124 , y = 220 , width = 48 , height = 16 },
	{ x = 180 , y = 220 , width = 64 , height = 24 },
	{ x = 251 , y = 220 , width = 64 , height = 27 },
	{ x = 147 , y = 261 , width = 26 , height = 22 },
	{ x = 178 , y = 262 , width = 27 , height = 21 },
	{ x = 210 , y = 264 , width = 27 , height = 19 },
	{ x = 16 , y = 258 , width = 59 , height = 30 }
  }
}

--Slider options and Slider labels
local sliderOpts = 
{
  mouthSlider = {id="mouth", x = 350, y = 150, width=300 };
  caudalSlider = {id="caudal", x = 350, y = 180, width=300};
  pelvicSlider = {id="pelvic", x = 350, y = 210, width=300};
  analSlider = {id="anal", x = 350, y = 240, width=300};
  softSlider = {id="softRay", x = 350, y = 270, width = 300};
  horizSlider = {id="horiz", x = 350, y = 300, width=300};

  mouthLabel = {text="Mouth", x= 150, y= 150, height=30};
  caudalLabel = {text="Caudal Fin", x=150, y=180, height=30};
  pelvicLabel = {text="Pelvic Fin", x=150, y=210, height=30};
  analLabel = {text="Anal Fin", x=150, y = 240, height=30};
  softLabel = {text="Soft Ray", x=150, y=270, height=30};
  horizLabel = {text="Horiz", x=150, y=300, height=30};
}

--Radio Button callback event
local function onSwitchPress(event)
  
  print("On switch press was called");
  if(isTouchMode and event.target.id == "sliders") then
    sliderCover:setFillColor(0,0,0,0);
    isTouchMode = false;
  else 
    if(isTouchMode == false and event.target.id == "touch") then
      sliderCover:setFillColor(0,0,0,1 );
      isTouchMode = true;
    end
  end

end

--Radio Button Options
local radioButtonOpts = {
  touchButton = {id="touch", style="radio", x = 75, y = 200, onPress = onSwitchPress, initialSwitchState=true};
  slidersButton = {id="sliders", style="radio", x = 75, y = 250, onPress = onSwitchPress};
  touchLabel = {text="Touch Mode", x = 10, y = 200, height = 50};
  sliderLabel = {text="Slider Mode", x = 10, y = 250, height = 50};
}

--Creating a new image sheet
imgSheet = graphics.newImageSheet("KingFossil.png", imgOpts);

--Settting up image sequence data for on touch
local touchImgSeq = 
{
  { name="body", start=1, count=1, time=500, loopCount=1 },
  { name="mouth", frames={2,3,4,5}, time=500, loopCount=1 },
  { name="caudal", frames={6,7,8,9,10}, time=500, loopCount=1 },
  { name="pelvic", frames={11,12,13}, time=500, loopCount=1 },
  { name="anal", frames={14,15}, time=500, loopCount=1 },
  { name="soft", frames={16,17,18}, time=500, loopCount=1},
  { name="other1", frames={19}, time=500, loopCount=1},

  { name="bodyBck", start=1, count=1, time=500, loopCount=1 },
  { name="mouthBck", frames={5,4,3,2}, time=500, loopCount=1},
  { name="caudalBck", frames={10,9,8,7,6}, time=500, loopCount=1},
  { name="pelvicBck", frames={13,12,11}, time=500, loopCount=1},
  { name="analBck", frames={15,14}, time=500, loopCount=1},
  { name="softBck", frames={18,17,16}, time=500, loopCount=1 },
  { name="other1Bck", frames={19}, time=500, loopCount=1}
}

--Tap functions for each part
local function onTap( event )
  --Disable touch functionality if in slider mode
  if(isTouchMode == false) then
    return;
  end

  print( "Sprite event: ", event.target.name, event.target.sequence);
  --Switching between backwards and forwards animations for touch mode
  if(event.target.name == event.target.sequence) then
  	event.target:setSequence( event.target.name.."Bck" );
  else
  	event.target:setSequence(event.target.name);
  end

  --Play animation and audio
  event.target:play();
  audio.play(event.target.audio);

end

--Create slider function
local function onSliderRelease( event )
  if(isTouchMode) then
    return;
  end

  --if event.phase == "ended" then
    local amnt = 1 + math.floor(event.target.value/100 * (event.target.frames - 1) + 0.5);
    event.target.part:setFrame(amnt);
  --end
end

--Create Horizontal movement slider
local function onHrzSlide( event )
  if(isTouchMode) then
    return;
  end

  local amnt = math.floor(event.target.value/100 * display.contentWidth + 0.5);
  event.target.part.x = amnt;
end

--Create sliders
local mthSlide = widget.newSlider(sliderOpts.mouthSlider);
mthSlide.frames = 4;
mthSlide:addEventListener("touch", onSliderRelease);
local cdlSlide = widget.newSlider(sliderOpts.caudalSlider);
cdlSlide.frames = 5;
cdlSlide:addEventListener("touch", onSliderRelease);
local plvSlide = widget.newSlider(sliderOpts.pelvicSlider);
plvSlide.frames = 3;
plvSlide:addEventListener("touch", onSliderRelease);
local anlSlide = widget.newSlider(sliderOpts.analSlider);
anlSlide.frames = 2;
anlSlide:addEventListener("touch", onSliderRelease);
local sftSlide = widget.newSlider(sliderOpts.softSlider);
sftSlide.frames = 3;
sftSlide:addEventListener("touch", onSliderRelease);
local hrzSlide = widget.newSlider(sliderOpts.horizSlider);
hrzSlide:addEventListener("touch", onHrzSlide);

--Create Slider Labels
display.newText(sliderOpts.mouthLabel);
display.newText(sliderOpts.caudalLabel);
display.newText(sliderOpts.pelvicLabel);
display.newText(sliderOpts.analLabel);
display.newText(sliderOpts.softLabel);
display.newText(sliderOpts.horizLabel);

--Create RadioButtons w/ labels
local radioButtonGroup = display.newGroup();

local radio1 = widget.newSwitch(radioButtonOpts.touchButton);
local radio1Text = display.newText(radioButtonOpts.touchLabel);
radioButtonGroup:insert(radio1);
radioButtonGroup:insert(radio1Text);

local radio2 = widget.newSwitch(radioButtonOpts.slidersButton);
local radio2Text = display.newText(radioButtonOpts.sliderLabel);
radioButtonGroup:insert(radio2);
radioButtonGroup:insert(radio2Text);

--Display Rect to cover the sliders if touch mdode is true
sliderCover = display.newRect(350, 230, 500, 200 );
sliderCover:setFillColor(0,0,0,1);

--Create group for body to translate
KingFossilGroup = display.newGroup();

--Using the image sheet to create static image of body
bodyImage = display.newSprite(imgSheet,touchImgSeq);
bodyImage.name="body";
bodyImage:setSequence("body");
KingFossilGroup:insert(bodyImage);
hrzSlide.part = KingFossilGroup;

--Using the image sheet to create dynamic image of mouth
mouthImage = display.newSprite(imgSheet,touchImgSeq);
mouthImage.name="mouth";
mouthImage.audio=audio.loadSound( "testSound1.wav");
mouthImage:setSequence("mouth");
--The anchor must be set to a constant point shared between the frames
mouthImage.anchorX=1.0;
mouthImage.anchorY=0.0;
mouthImage:translate(-30,-2);
--Initial frame 1
mouthImage:setFrame(1);
--Adding the sound tapping functionality
mouthImage:addEventListener("tap",onTap);
KingFossilGroup:insert(mouthImage);
--Adding the mouth sprite to the slider functionality
mthSlide.part = mouthImage;

--Using the image sheet to create dynamic image of the caudal fin, repeating the mouth
caudalImage = display.newSprite(imgSheet,touchImgSeq);
caudalImage.name="caudal";
caudalImage.audio=audio.loadSound("testSound2.wav");
caudalImage:setSequence("caudal");
caudalImage:translate(73,7);
caudalImage.anchorX=0.0
caudalImage:setFrame(1);
caudalImage:addEventListener("tap",onTap);
KingFossilGroup:insert(caudalImage);
cdlSlide.part = caudalImage;

--Using the image sheet to create dynamic image of the pelvic fin
pelvicImage = display.newSprite(imgSheet, touchImgSeq);
pelvicImage.name="pelvic";
pelvicImage.audio=audio.loadSound("testSound3.wav");
pelvicImage:setSequence("pelvic");
pelvicImage:translate(-6,22);
pelvicImage.anchorY=0.0;
pelvicImage:setFrame(1);
pelvicImage:addEventListener("tap",onTap);
KingFossilGroup:insert(pelvicImage);
plvSlide.part = pelvicImage;

--Using the image sheet to create dynamic image of the anal fin
analImage = display.newSprite(imgSheet, touchImgSeq);
analImage.name="anal";
analImage.audio=audio.loadSound("testSound4.wav");
analImage:setSequence("anal");
analImage:translate(33,22);
analImage.anchorX=0.0;
analImage.anchorY=0.0;
analImage:setFrame(1);
analImage:addEventListener("tap",onTap);
KingFossilGroup:insert(analImage);
anlSlide.part = analImage;

--Using the image sheet to create dynamic image of the soft ray
softImage = display.newSprite(imgSheet, touchImgSeq);
softImage.name="soft";
softImage.audio=audio.loadSound("testSound5.wav");
softImage:setSequence("soft");
softImage:translate(53,-10);
softImage.anchorY = 1.0;
softImage.anchorX = 0.0;
softImage:setFrame(1);
softImage:addEventListener("tap",onTap);
KingFossilGroup:insert(softImage);
sftSlide.part = softImage;

--Using the image sheet to create static image of the other part
other1Image = display.newSprite(imgSheet, touchImgSeq);
other1Image.name="other1";
other1Image.anchorY = 1.0;
other1Image:setSequence("other1");
other1Image:translate(36,-19);
KingFossilGroup:insert(other1Image);

KingFossilGroup:translate(display.contentWidth/2,display.contentHeight/4)
--KingFossilGroup:scale(4.0,4.0);
